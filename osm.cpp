#include <iostream>
#include <sys/time.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <bits/local_lim.h>
#include "osm.h"
#include <cstdlib>

using namespace std;

/* Defines */
#define MILISEC_FACTOR 1000000
#define MILI_TO_NANO 1000
#define RELATIVE_PATH "temp.txt"
#define ABSOLUTE_PATH_FOR_TEMP_FILE "/tmp/temp.txt"
#define FLAGS O_DIRECT | O_SYNC | O_CREAT
#define MODE 0777 //Instead of O_RDWR.
#define FAIL_VAL -1
#define UNROLLING 10
#define FIRST_VAL (char)10
#define SECOND_VAL (char)30
#define NULL_STRING '\0'
#define TEST_STRING "a"
#define OK_STATUS 0



/* Global Variables */
char *host_name;

/* Helper Functions */
void empty_function()
{ } //Empty function for 'osm_function_time' testing.
/*
 * Function for dealing with bad output results
 */
void calcMeasureTimesDiv(double dividend, double divisor, double *result)
{
    if ((divisor == 0) || (divisor == (-1)) || (dividend == (-1)))
    {
        *result = (-1);
    } else
    {
        *result = dividend / divisor;
    }
}

/* END Helper funcs */

/* 'osm.h' Implementation */
timeMeasurmentStructure measureTimes(unsigned int operation_iterations,
                                     unsigned int function_iterations,
                                     unsigned int syscall_iterations,
                                     unsigned int disk_iterations)
{
    timeMeasurmentStructure p_results_struct;
    int status = OK_STATUS;

    status = gethostname(host_name, HOST_NAME_MAX);

    if (status == FAIL_VAL)
    {
        p_results_struct.machineName = NULL_STRING;
    } else
    {
        p_results_struct.machineName = host_name;
    }

    p_results_struct.instructionTimeNanoSecond =
            osm_operation_time(operation_iterations);
    p_results_struct.functionTimeNanoSecond =
            osm_function_time(function_iterations);
    p_results_struct.trapTimeNanoSecond = osm_syscall_time(syscall_iterations);
    p_results_struct.diskTimeNanoSecond = osm_disk_time(disk_iterations);

    calcMeasureTimesDiv(p_results_struct.functionTimeNanoSecond,
                        p_results_struct.instructionTimeNanoSecond,
                        &p_results_struct.functionInstructionRatio);

    calcMeasureTimesDiv(p_results_struct.trapTimeNanoSecond,
                        p_results_struct.instructionTimeNanoSecond,
                        &p_results_struct.trapInstructionRatio);

    calcMeasureTimesDiv(p_results_struct.diskTimeNanoSecond,
                        p_results_struct.instructionTimeNanoSecond,
                        &p_results_struct.diskInstructionRatio);
    return p_results_struct;
}

/* Initialization function that the user must call
 * before running any other library function.
 * The function may, for example, allocate memory or
 * create/open files.
 * Returns 0 uppon success and -1 on failure
 */
int osm_init()
{
    host_name = (char *) malloc(HOST_NAME_MAX);
    if (host_name == NULL)
    {
        free(host_name);
        return FAIL_VAL;
    }
    return 0;
}


/* finalizer function that the user must call
 * after running any other library function.
 * The function may, for example, free memory or
 * close/delete files.
 * Returns 0 uppon success and -1 on failure
 */
int osm_finalizer()
{
    free(host_name);
    return 0;
}


/* Time measurement function for a simple arithmetic operation.
   returns time in nano-seconds upon success,
   and -1 upon failure.
   */
double osm_operation_time(unsigned int iterations)
{
    //Variables.
    unsigned int index = 0;
    struct timeval time;
    double before, after, result;
    char a, b, c, d, e, f, g, h, i, j;
    a = 0;
    b = 0;
    c = 0;
    d = 0;
    e = 0;
    f = 0;
    g = 0;
    h = 0;
    i = 0;
    j = 0;

    (iterations == 0) ? iterations = 1000 : iterations;

    //Get time in milliseconds before the test.
    result = gettimeofday(&time, NULL);
    if (result != FAIL_VAL) //Upon 'gettimeofday' failure.
    {
        before = (double) (time.tv_sec * MILISEC_FACTOR + time.tv_usec);
        for (index = 0; index < iterations; index += UNROLLING)
        {
            //10 arbitrary bitwise operations
            a = a ^ SECOND_VAL;
            b = FIRST_VAL << b;
            c = c >> SECOND_VAL;
            d = FIRST_VAL | d;
            e = e & SECOND_VAL;
            f = FIRST_VAL ^ f;
            g = g >> SECOND_VAL;
            h = FIRST_VAL << h;
            i = i | SECOND_VAL;
            j = j & SECOND_VAL;
        }
    }

    //Get time in milliseconds after the test.
    if (result != FAIL_VAL) //Upon 'test_func' failure.
    {
        result = gettimeofday(&time, NULL);
        after = (double) (time.tv_sec * MILISEC_FACTOR + time.tv_usec);
    }

    //Returns the average of the measured time in nanoseconds.
    if (result != FAIL_VAL) //Upon 'gettimeofday' failure.
    {
        //Devide by index because of the UNROLLING.
        result = ((after - before) * MILI_TO_NANO) / index;
    }
    return result;
}


/* Time measurement function for an empty function call.
   returns time in nano-seconds upon success,
   and -1 upon failure.
   */
double osm_function_time(unsigned int iterations)
{
    //Variables.
    unsigned int i = 0;
    struct timeval time;
    double before, after, result;

    (iterations == 0) ? iterations = 1000 : iterations;

    //Get time in milliseconds before the test.
    result = gettimeofday(&time, NULL);
    if (result != FAIL_VAL) //Upon 'gettimeofday' failure.
    {
        before = (double) (time.tv_sec * MILISEC_FACTOR + time.tv_usec);
        for (i = 0; i < iterations; ++i)
        {
            empty_function();
        }
    }

    //Get time in milliseconds after the test.
    if (result != FAIL_VAL) //Upon 'test_func' failure.
    {
        result = gettimeofday(&time, NULL);
        after = (double) (time.tv_sec * MILISEC_FACTOR + time.tv_usec);
    }

    //Returns the average of the measured time in nanoseconds.
    if (result != FAIL_VAL) //Upon 'gettimeofday' failure.
    {
        result = ((after - before) * MILI_TO_NANO) / iterations;
    }
    return result;
}


/* Time measurement function for an empty trap into the operating system.
   returns time in nano-seconds upon success,
   and -1 upon failure.
   */
double osm_syscall_time(unsigned int iterations)
{
    //Variables.
    unsigned int i = 0;
    struct timeval time;
    double before, after, result;

    (iterations == 0) ? iterations = 1000 : iterations;

    //Get time in milliseconds before the test.
    result = gettimeofday(&time, NULL);
    if (result != FAIL_VAL) //Upon 'gettimeofday' failure.
    {
        before = (double) (time.tv_sec * MILISEC_FACTOR + time.tv_usec);
        for (i = 0; i < iterations; ++i)
        {
            OSM_NULLSYSCALL;
        }
    }

    //Get time in milliseconds after the test.
    if (result != FAIL_VAL) //Upon 'test_func' failure.
    {
        result = gettimeofday(&time, NULL);
        after = (double) (time.tv_sec * MILISEC_FACTOR + time.tv_usec);
    }

    //Returns the average of the measured time in nanoseconds.
    if (result != FAIL_VAL) //Upon 'gettimeofday' failure.
    {
        result = ((after - before) * MILI_TO_NANO) / iterations;
    }
    return result;
}

/* Time measurement function for accessing the disk.
   returns time in nano-seconds upon success,
   and -1 upon failure.
   */
double osm_disk_time(unsigned int iterations)
{
    //Variables.
    unsigned int i = 0;
    struct timeval time;
    double before, after, resultForTime, resultForFileOp = 0;
    int fd;
    (iterations == 0) ? iterations = 1000 : iterations;

    //Get time in milliseconds before the test.
    resultForTime = gettimeofday(&time, NULL);
    if (resultForTime == FAIL_VAL)
    {
        return FAIL_VAL;
    }
    if (resultForTime != FAIL_VAL)
    {
        before = (double) (time.tv_sec * MILISEC_FACTOR + time.tv_usec);

        for (i = 0; i < iterations; ++i)
        {
            fd = open(ABSOLUTE_PATH_FOR_TEMP_FILE, FLAGS, MODE);
            if (fd >= 0)
            {
//                write(fd, TEST_STRING, 1);

            } else
            {
                close(fd);
                remove(ABSOLUTE_PATH_FOR_TEMP_FILE);
                return FAIL_VAL;
            }
            close(fd);
            resultForFileOp = remove(ABSOLUTE_PATH_FOR_TEMP_FILE);
            if (resultForFileOp == FAIL_VAL)
            {
                return FAIL_VAL;
            }
        }

    }


    resultForTime = gettimeofday(&time, NULL);
    if (resultForTime == FAIL_VAL)
    {
        return FAIL_VAL;
    }

    after = (double) (time.tv_sec * MILISEC_FACTOR + time.tv_usec);


    //Returns the average of the measured time in nanoseconds.

    return ((after - before) * MILI_TO_NANO) / iterations;

}

/* main function for testing */
int main()
{
    osm_init();
    timeMeasurmentStructure p_results_struct =
            measureTimes(10000, 10000, 10000, 5000);

    cout << "Machine Name: " << p_results_struct.machineName << endl;
    cout << "Empty Func: " << p_results_struct.functionTimeNanoSecond
    << " ns" << endl;
    cout << "Sys Call: " << p_results_struct.trapTimeNanoSecond
    << " ns" << endl;
    cout << "Operation: " << p_results_struct.instructionTimeNanoSecond
    << " ns" << endl;
    cout << "HardDrive: " << p_results_struct.diskTimeNanoSecond
    << " ns" << endl;
    cout << "Func / Operation: " << p_results_struct.functionInstructionRatio
    << " ns" << endl;
    cout << "Sys Call / Operation: " << p_results_struct.trapInstructionRatio
    << " ns" << endl;
    cout << "HardDrive / Operation: " << p_results_struct.diskInstructionRatio
    << " ns" << endl;
    osm_finalizer();
    return 0;
}