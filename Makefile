CC=g++
RANLIB=ranlib
LIBSRC=osm.cpp 
LIBOBJ=osm.o
# INCS=-I.
CFLAGS = -Wall -O0
# LOADLIBES = -L./ 
OSMLIB = libosm.a
TARGETS = libosm.a
TAR=tar
TARFLAGS=-cvf
TARNAME=ex1.tar
TARSRCS=$(LIBSRC) Makefile README

.PHONY: all, clean, tar

all: libosm.a

osm.o: osm.cpp
	$(CC) $(CFLAGS) -c osm.cpp -o osm.o

libosm.a: osm.o
	$(AR) $(ARFLAGS) $@ $^
	$(RANLIB) $@

clean:
	$(RM) $(TARGETS) $(OSMLIB) $(OBJ) $(LIBOBJ) *~ *core

tar:
	$(TAR) $(TARFLAGS) $(TARNAME) $(TARSRCS)